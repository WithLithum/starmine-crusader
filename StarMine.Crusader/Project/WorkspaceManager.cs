﻿namespace StarMine.Crusader.Project;

using Microsoft.Extensions.Configuration;
using Serilog;
using StarMine.Crusader.Network.RetroMcp;

internal class WorkspaceManager
{
    private readonly RetroMcpManager _mcpManager;

    public WorkspaceManager(string workspace, RetroMcpManager mcpManager)
    {
        WorkspacePath = workspace;
        _mcpManager = mcpManager;
    }

    public string WorkspacePath { get; }
}
