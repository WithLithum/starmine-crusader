﻿namespace StarMine.Crusader;

using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using StarMine.Crusader.Java;
using StarMine.Crusader.Network.RetroMcp;
using StarMine.Crusader.Tasks;

internal static class Program
{
    internal static readonly IConfiguration Config = new ConfigurationBuilder()
        .AddJsonFile("settings.json")
        .Build();

    static async Task<int> Main(string[] args)
    {
        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console(theme: AnsiConsoleTheme.Code,
            outputTemplate: "[{Level:u3}] {Message:lj}{NewLine}{Exception}")
            .CreateLogger();

        Directory.CreateDirectory(CrusaderApp.AppPath);
        Directory.CreateDirectory(CrusaderApp.McpPath);

        var app = new CrusaderApp(Config);

        Log.Information("Starting pre-app setup procedure");

        var javaTask = new SetupJavaTask(app);
        if (!await ConsoleTasks.ExecuteInteractiveAsync(javaTask))
        {
            return -1;
        }

        var provider = javaTask.Result!;

        var mcpTask = new SetupMcpTask(app, provider);
        if (!await ConsoleTasks.ExecuteInteractiveAsync(mcpTask))
        {
            return -1;
        }

        return 0;
    }
}
