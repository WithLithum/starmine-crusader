﻿namespace StarMine.Crusader.Tasks;

using Serilog;
using StarMine.Crusader.Java;
using System.Threading.Tasks;

public class SetupJavaTask : ITask
{
    internal SetupJavaTask(CrusaderApp app)
    {
        _app = app;
    }

    private readonly CrusaderApp _app;

    public string Name => "setupJava";

    public bool UseSystemJava { get; set; }

    public IJavaProvider? Result { get; private set; }

    public async Task ExecuteAsync()
    {
        var searcher = _app.SystemJavaSearcher;
        var cacheManager = _app.JavaCacheManager;
        var installer = _app.JavaInstaller;

        if (UseSystemJava && await searcher.IsSystemJavaValid())
        {
            Log.Information("Detected a valid Java 8 installed in the system.");
            Result = searcher;
        }
        else if (await cacheManager.IsCachedJavaValid()
            || await installer.TryInstallJavaAsync())
        {
            Log.Information("Will use cached Java.");
            Result = cacheManager;
        }
        else
        {
            Log.Information("Failed to install Java.");
            throw new OperationCanceledException("Unable to install Java.");
        }
    }
}
