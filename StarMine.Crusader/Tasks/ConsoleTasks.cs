﻿namespace StarMine.Crusader.Tasks;

using Serilog;
using Spectre.Console;
using System;
using System.Threading.Tasks;

internal static class ConsoleTasks
{
    public static async Task<bool> ExecuteInteractiveAsync(ITask task)
    {
        AnsiConsole.MarkupLineInterpolated($"[steelblue1_1]Task [/][aquamarine1]:{task.Name}[/]");

        try
        {
            await task.ExecuteAsync();
        }
        catch (OperationCanceledException ex)
        {
            Log.Error("Task failed: {Message}", ex.Message);
            return false;
        }

        return true;
    }
}
