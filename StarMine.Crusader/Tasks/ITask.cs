﻿namespace StarMine.Crusader.Tasks;
using System.Threading.Tasks;

public interface ITask
{
    string Name { get; }

    Task ExecuteAsync();
}
