﻿namespace StarMine.Crusader.Tasks;

using StarMine.Crusader.Java;
using StarMine.Crusader.Network.RetroMcp;
using System.Threading.Tasks;

internal class DecompileTask : ITask
{
    internal DecompileTask(RetroMcpManager manager, IJavaProvider provider)
    {
        _manager = manager;
        _provider = provider;
    }


    private readonly RetroMcpManager _manager;
    private readonly IJavaProvider _provider;

    public string Name => "decompileMcp";

    public async Task ExecuteAsync()
    {
        var result = await _manager.ExecuteMcp("decompile server", _manager.WorkDirectory, _provider);
        if (!result.IsSuccess)
        {
            throw new OperationCanceledException("Failed to decompile Minecraft!");
        }
    }
}
