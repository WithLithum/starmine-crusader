﻿namespace StarMine.Crusader.Tasks;

using Serilog;
using StarMine.Crusader.Java;
using System;
using System.Threading.Tasks;

internal class SetupMcpTask : ITask
{
    private readonly IJavaProvider _provider;
    private readonly CrusaderApp _app;

    public SetupMcpTask(CrusaderApp app, IJavaProvider provider)
    {
        _app = app;
        _provider = provider;
    }

    public string Name => "setupMcp";

    public async Task ExecuteAsync()
    {
        if (_app.RetroMcpManager.IsSetupComplete())
        {
            Log.Information("Existing RetroMCP setup found.");
            return;
        }

        var result = await _app.RetroMcpManager.TrySetupInteractiveAsync(_provider);

        if (!result)
        {
            throw new OperationCanceledException("Unable to setup RetroMCP.");
        }
    }
}
