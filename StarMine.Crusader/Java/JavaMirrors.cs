﻿namespace StarMine.Crusader.Java;
using Microsoft.Extensions.Configuration;
using Serilog;
using Spectre.Console;
using StarMine.Crusader.Network;
using System.Net.NetworkInformation;

internal static class JavaMirrors
{
    internal static async Task<string?> GetBestMirror(IEnumerable<KeyValuePair<string, string?>> mirrors)
    {
        var ping = new Ping();
        var pingTimes = new Dictionary<string, long>();

        foreach (var mirror in mirrors)
        {
            if (mirror.Key == "JavaMirrors")
            {
                continue;
            }

            Log.Information("Evaluating mirror {Key}", mirror.Key);
            var realKey = mirror.Key[12..];

            try
            {
                if (mirror.Value == null)
                {
                    Log.Warning("Mirror has no value - check if you didn't write one?");
                    Log.Warning("Skipped mirror {Key}.", mirror.Key);
                    continue;
                }

                var pr = await ping.SendPingAsync(realKey);

                if (pr.Status != IPStatus.Success)
                {
                    Log.Warning("Ping mirror {RealKey}: failure - {Status}", realKey, pr.Status);
                    continue;
                }

                Log.Information("Ping mirror {RealKey}: success - {RoundtripTime}ms", realKey, pr.RoundtripTime);
                pingTimes.Add(mirror.Value, pr.RoundtripTime);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to ping mirror {RealKey}: ", realKey);
                AnsiConsole.WriteException(ex);
            }
        }

        if (pingTimes.Count == 0)
        {
            return null;
        }

        var best = pingTimes.Aggregate((x, y) => x.Value > y.Value ? x : y);
        Log.Information("Best mirror: {Key} ({Value}ms)", best.Key, best.Value);
        return best.Key;
    }

    internal static IEnumerable<KeyValuePair<string, string?>> LoadMirrors()
    {
        return Program.Config.GetRequiredSection("JavaMirrors").AsEnumerable();
    }

    internal static async Task<string?> TryGetBestMirrorUrlAsync(JavaBinaryData javaInfo)
    {
        var mirrors = LoadMirrors();
        if (mirrors == null)
        {
            Log.Error("No mirrors configured.");
            return null;
        }

        var bestMirror = await GetBestMirror(mirrors);
        if (bestMirror == null)
        {
            Log.Error("Cannot find at least one mirror available.");
            return null;
        }

        return AdoptiumHelper.FormatMirrorUri(bestMirror,
            javaInfo.Os,
            javaInfo.Package.Name,
            javaInfo.Package.Link);
    }
}