﻿namespace StarMine.Crusader.Java;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IJavaProvider
{
    ValueTask<JavaExecutionResult> ExecuteJavaAsync(string arguments, string? workDir = null);
}
