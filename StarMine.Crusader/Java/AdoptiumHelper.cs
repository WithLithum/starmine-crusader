﻿namespace StarMine.Crusader.Java;

using Downloader;
using Microsoft.Extensions.Configuration;
using Serilog;
using StarMine.Crusader.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

internal static class AdoptiumHelper
{
    public static string ApiRoot => Program.Config.GetValue("ApiRoot", "https://api.adoptium.net/")
        ?? "https://api.adoptium.net/";

    public const int TargetFeatureRelease = 8;

    private static readonly JsonSerializerOptions _jsonOptions = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower
    };

    private static readonly HttpClient _httpClient = new HttpClient()
    {
        DefaultRequestHeaders =
        {
            UserAgent =
            {
                new System.Net.Http.Headers.ProductInfoHeaderValue("starmine-crusader", "0.1.0-alpha")
            }
        }
    };

    internal static async Task<JavaBinaryData?> GetLatestJava()
    {
        // Calls the adoptium API to get the latest Java version
        // Only one in 1 page.
        var result = await _httpClient.GetAsync($"{ApiRoot}v3/assets/feature_releases/{TargetFeatureRelease}/ga?heap_size=normal&image_type=jdk&" +
            $"os={SystemHelper.GetSystemType()}&page=0&page_size=1&project=jdk&sort_method=DEFAULT&sort_order=DESC&vendor=eclipse");

        // Check the first and only one metadata.
        // If the metadata is invalid.
        if (!result.IsSuccessStatusCode)
        {
            Log.Error("Failed to get Java information: {StatusCode}", result.StatusCode.ToString("GD"));
            return null;
        }

        var array = await result.Content.ReadFromJsonAsync<JavaReleaseMetadata[]>(_jsonOptions);
        if (array == null || array.Length == 0)
        {
            return null;
        }

        var meta = array[0];
        Log.Information("Selected version {ReleaseName} with ID {Id}.", meta.ReleaseName, meta.Id);

        if (meta.Binaries.Count == 0)
        {
            Log.Error("No binaries available");
            return null;
        }
        return meta.Binaries[0];
    }

    internal static string FormatMirrorUri(string format, string platform, string fileName, Uri original)
    {
        return string.Format(format, platform, fileName, original);
    }
}
