﻿namespace StarMine.Crusader.Java;

using Downloader;
using Serilog;
using Spectre.Console;
using System;
using System.Threading.Tasks;

[Obsolete("Use AssetDownloader instead.")]
internal class JavaDownloader
{
    private readonly ProgressContext _context;
    private ProgressTask? _task;
    private bool complete;
    private readonly string _url;
    private readonly string _target;

    internal JavaDownloader(string url, string target, ProgressContext context)
    {
        _url = url;
        _target = target;
        _context = context;
    }

    private static readonly DownloadConfiguration downloadOpt = new()
    {
        ChunkCount = 5,
        ParallelDownload = true,
        CheckDiskSizeBeforeDownload = true,
        RequestConfiguration =
        {
            UserAgent = "starmine-crusader/0.1.0-alpha"
        }
    };

    internal async Task<bool> DownloadIntercativeAsync()
    {
        var downloader = new DownloadService(downloadOpt);

        _task = _context.AddTask("Downloading");
        downloader.DownloadProgressChanged += Downloader_DownloadProgressChanged;
        downloader.DownloadFileCompleted += Downloader_DownloadFileCompleted;

        Log.Information("Downloading ({0}) to ({1})", _url, _target);

        try
        {
            File.WriteAllBytes(_target, []);
            await downloader.DownloadFileTaskAsync(_url, _target);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Failed to download {Url}", _url);
            return false;
        }

        while (!complete)
        {
            // do nothing
        }

        return true;
    }

    private void Downloader_DownloadFileCompleted(object? sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        _task?.StopTask();
        _task = null;
        complete = true;
    }

    private void Downloader_DownloadProgressChanged(object? sender, DownloadProgressChangedEventArgs e)
    {
        if (_task == null)
        {
            return;
        }

        _task.Value = e.ProgressPercentage;
    }
}
