﻿namespace StarMine.Crusader.Java;

using Serilog;
using Spectre.Console;
using StarMine.Crusader.Network;
using System;
using System.Diagnostics;
using System.Runtime.Versioning;

public static class SystemHelper
{
    public static string SystemJavaName { get; set; } = "java";
    public static string? JavaHome { get; set; }
    public static string? Classpath { get; set; }
    public static string? JdkHome { get; set; }

    public const string VersionCall = "-version";

    public static readonly string CacheJavaPath = Path.Combine(
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        "starmine",
        "crusader",
        "java"
        );

    public static readonly string CacheJavaMagicFile = Path.Combine(
       Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
       "starmine",
       "crusader",
       "java",
       "installed.txt"
       );

    public const string Alpine = "ID=alpine";

    public static string? GetCachedJavaPath()
    {
        if (!File.Exists(CacheJavaMagicFile))
        {
            return null;
        }

        return Path.Combine(CacheJavaPath, File.ReadAllText(CacheJavaMagicFile));
    }

    public static string? GetCachedJavaBin()
    {
        var path = GetCachedJavaPath();
        if (path == null)
        {
            return null;
        }

        if (OperatingSystem.IsWindows())
        {
            return Path.Combine(path, "bin\\java.exe");
        }
        else
        {
            return Path.Combine(path, "bin/java");
        }
    }

    [SupportedOSPlatform("linux")]
    public static bool IsAlpine()
    {
        using var osRelease = new StreamReader(File.OpenRead("/etc/os-release"));
        while (true)
        {
            var line = osRelease.ReadLine();

            if (line == null
                || (line.StartsWith("ID=")
                && line != Alpine))
            {
                return false;
            }

            if (line == Alpine)
            {
                return true;
            }
        }
    }

    public static string GetSystemType()
    {
        if (OperatingSystem.IsWindows())
        {
            return "windows";
        }
        else if (OperatingSystem.IsLinux())
        {
            // Alpine uses musl, and we must be aware about this.
            if (IsAlpine())
            {
                return "alpine-linux";
            }

            return "linux";
        }
        else if (OperatingSystem.IsMacOS())
        {
            return "mac";
        }

        throw new NotSupportedException("Unsupported operating system!");
    }

    public static string GetJavaVersionSlug8(JavaVersionData data)
    {
        return $"jdk{data.Major}u{data.Security}-b{data.Build}";
    }

    public static async Task<bool> IsJava8Async(ProcessStartInfo processInfo, CancellationToken token = default)
    {
        processInfo.RedirectStandardError = true;

        Process? process;
        try
        {
            process = Process.Start(processInfo);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Failed to acquire system Java information!");
            return false;
        }

        if (process == null)
        {
            return false;
        }

        await process.WaitForExitAsync(token);

        var line = process.StandardError.ReadLine();
        if (line == null)
        {
            process.StandardError.Dispose();
            return false;
        }

        // An example Java output would be like [openjdk version "17.0.9" 2023-10-17]
        // So we select the third one
        var split = line.Split(' ');
        if (split.Length < 3)
        {
            Log.Error("The Java present on the system may not be a standard Java.");
            return false;
        }

        Log.Information("Determined Java version: {0}", split[2]);
        process.StandardError.Dispose();
        return split[2].StartsWith("\"1.8");
    }

    public static async Task<bool> TryExecuteJavaAsync(string javaName, string arguments, string javaHome)
    {
        var processInfo = CreateStartInfo(javaName, javaHome, arguments);

        Process? process;
        try
        {
            process = Process.Start(processInfo);
        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
            return false;
        }

        if (process == null)
        {
            return false;
        }

        await process.WaitForExitAsync();
        return true;
    }

    public static async ValueTask<JavaExecutionResult> ExecuteJavaAsync(string javaName, string arguments, string javaHome, string? workDir = null)
    {
        var processInfo = CreateStartInfo(javaName, javaHome, arguments, x =>
        {
            if (workDir != null)
            {
                x.WorkingDirectory = workDir;
            }
        });

        Process? process;
        try
        {
            process = Process.Start(processInfo);
        }
        catch (Exception e)
        {
            Log.Error(e, "Failed to execute Java");
            return JavaExecutionResult.StartFailure();
        }

        if (process == null)
        {
            return JavaExecutionResult.StartFailure();
        }

        await process.WaitForExitAsync();
        return new(true, process.ExitCode);
    }

    public static string GetRandomFileName()
    {
        return Path.Combine(Directory.GetCurrentDirectory(), Path.GetRandomFileName());
    }

    internal static ProcessStartInfo CreateStartInfo(string binary,
        string path,
        string arguments,
        Action<ProcessStartInfo>? configure = null)
    {
        var startInfo = new ProcessStartInfo(binary, arguments);
        startInfo.EnvironmentVariables["JAVA_HOME"] = path;
        startInfo.EnvironmentVariables["CLASSPATH"] = SystemJavaSearcher.Classpath;
        configure?.Invoke(startInfo);
        return startInfo;
    }
}
