﻿namespace StarMine.Crusader.Java;

using System;
using System.Diagnostics;

internal class SystemJavaSearcher : IJavaProvider
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SystemJavaSearcher"/> class.
    /// </summary>
    /// <param name="javaCacheDirectory">The location to store downloaded Java.</param>
    /// <param name="systemJavaName">The name of the executable of the system Java.</param>
    public SystemJavaSearcher(string systemJavaName = "java", string? systemJavaHome = null)
    {
        SystemJavaName = systemJavaName;
        SystemJavaHome = systemJavaHome
            ?? Environment.GetEnvironmentVariable("JAVA_HOME")
            ?? throw new ArgumentException("Java Home not present on system, in this case JavaHome must be set", nameof(systemJavaHome));
    }

    public string SystemJavaName { get; }

    public string SystemJavaHome { get; }

    public const string Classpath = @".;%JAVA_HOME%\\lib\\dt.jar;%JAVA_HOME%\\lib\\tools.jar;";

    public async Task<bool> IsSystemJavaValid(CancellationToken token = default)
    {
        return await SystemHelper.IsJava8Async(CreateStartInfo(SystemHelper.VersionCall), token);
    }

    public async ValueTask<JavaExecutionResult> ExecuteJavaAsync(string arguments, string? workDir = null)
    {
        return await SystemHelper.ExecuteJavaAsync(SystemJavaName,
            arguments,
            SystemJavaHome,
            workDir);
    }

    public async Task<bool> TryExecuteJavaAsync(string arguments)
    {
        return await SystemHelper.TryExecuteJavaAsync(SystemJavaName,
            arguments,
            SystemJavaHome);
    }

    private ProcessStartInfo CreateStartInfo(string arguments, Action<ProcessStartInfo>? configure = null)
    {
        return SystemHelper.CreateStartInfo(SystemJavaName, SystemJavaHome, arguments, configure);
    }
}
