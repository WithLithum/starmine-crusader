﻿namespace StarMine.Crusader.Java;

using Serilog;
using SharpCompress.Archives;
using SharpCompress.Archives.Tar;
using SharpCompress.Archives.Zip;
using SharpCompress.Common;
using SharpCompress.Readers;
using Spectre.Console;
using StarMine.Crusader.Network;
using System.Runtime.Versioning;
using System.Security.Cryptography;

public class JavaInstaller
{
    private readonly JavaCacheManager _cacheManager;
    private readonly DownloadManager _downloadManager;

    internal static readonly string DefaultDownloadsDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        "starmine", "crusader", "downloads");

    internal JavaInstaller(JavaCacheManager cacheManager, DownloadManager downloadManager)
    {
        _cacheManager = cacheManager;
        _downloadManager = downloadManager;
    }

    [SupportedOSPlatform("windows")]
    internal bool TryExtractJavaWindows(string fileName)
    {
        try
        {
            using var archive = ZipArchive.Open(fileName);
            var dir = archive.Entries.First(x => x.IsDirectory);
            File.WriteAllText(_cacheManager.MagicFile, dir.Key);

            var progress = new Progress(AnsiConsole.Console);
            progress.Start((context) =>
            {
                var task = context.AddTask("Extracting Java");
                archive.ExtractToDirectory(_cacheManager.JavaCacheDirectory, x => task.Value = x);
            });
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Failed to extract Java");
            return false;
        }

        return true;
    }

    [UnsupportedOSPlatform("windows")]
    internal bool TryExtractJavaGZip(string fileName)
    {
        try
        {
            using var archive = TarArchive.Open(fileName);
            var dir = archive.Entries.First(x => x.IsDirectory);
            File.WriteAllText(_cacheManager.MagicFile, dir.Key);

            var progress = new Progress(AnsiConsole.Console);
            progress.Start((context) =>
            {
                var task = context.AddTask("Extracting Java");
                archive.ExtractToDirectory(_cacheManager.JavaCacheDirectory, x => task.Value = x);
            });
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Failed to extract Java");
            return false;
        }

        return true;
    }

    internal async Task<bool> TryInstallJavaAsync()
    {
        Directory.CreateDirectory(DefaultDownloadsDir);

        var javaInfo = await AdoptiumHelper.GetLatestJava();
        if (javaInfo == null)
        {
            return false;
        }

        var temp = Path.Combine(_cacheManager.DownloadDirectory, javaInfo.Package.Name);
        bool succeed = await DownloadAsync(javaInfo);

        if (!succeed)
        {
            return false;
        }

        if (OperatingSystem.IsWindows())
        {
            if (!TryExtractJavaWindows(temp))
            {
                return false;
            }

            return true;
        }
        else
        {
            if (!TryExtractJavaGZip(temp))
            {
                return false;
            }

            return true;
        }
    }

    private async Task<bool> DownloadAsync(JavaBinaryData javaInfo)
    {
        if (await _downloadManager.DoesAssetExistAsync(javaInfo.Package.Name, javaInfo.Package.Link.ToString()))
        {
            return true;
        }

       
        var mirrorUrl = await JavaMirrors.TryGetBestMirrorUrlAsync(javaInfo);
        if (mirrorUrl == null)
        {
            return false;
        }

        var asset = new NetworkAsset()
        {
            FileName = javaInfo.Package.Name,
            Link = mirrorUrl,
            Sha256 = javaInfo.Package.Checksum
        };

        // Download a new file.
        return await _downloadManager.TryResolveAssetInteractiveAsync(asset);
    }
}
