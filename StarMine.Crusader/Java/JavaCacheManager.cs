﻿namespace StarMine.Crusader.Java;
internal class JavaCacheManager : IJavaProvider
{
    public JavaCacheManager(string? javaCacheDirectory = null, string? downloadDirectory = null)
    {
        JavaCacheDirectory = javaCacheDirectory
            ?? SystemHelper.CacheJavaPath;
        MagicFile = Path.Combine(JavaCacheDirectory, "installed.txt");
        DownloadDirectory = downloadDirectory
            ?? JavaInstaller.DefaultDownloadsDir;
    }

    public string JavaCacheDirectory { get; }

    public string MagicFile { get; }

    public string DownloadDirectory { get; }

    /// <summary>
    /// Gets the cached Java installation location.
    /// </summary>
    /// <returns>The cached Java location; if not found, returns <see langword="null"/>.</returns>
    public string? GetCachedJavaPath()
    {
        if (!File.Exists(MagicFile))
        {
            return null;
        }

        return Path.Combine(JavaCacheDirectory, File.ReadAllText(MagicFile));
    }

    /// <summary>
    /// Gets the path to the cached Java binary location.
    /// </summary>
    /// <param name="name">The name of the executable to locate.</param>
    /// <returns>The cached Java binary location; if not found, returns <see langword="null"/>.</returns>
    public string? GetCachedJavaBin(string name = "java")
    {
        var path = GetCachedJavaPath();
        if (path == null)
        {
            return null;
        }

        if (OperatingSystem.IsWindows())
        {
            return Path.Combine(path, $"bin\\{name}.exe");
        }
        else
        {
            return Path.Combine(path, $"bin/{name}");
        }
    }

    public async ValueTask<JavaExecutionResult> ExecuteJavaAsync(string arguments, string? workDir = null)
    {
        var path = GetCachedJavaPath();
        var bin = GetCachedJavaBin();
        if (path == null || bin == null)
        {
            return JavaExecutionResult.StartFailure();
        }

        return await SystemHelper.ExecuteJavaAsync(bin,
            arguments,
            path,
            workDir);
    }

    public async Task<bool> TryExecuteJavaAsync(string arguments)
    {
        var path = GetCachedJavaPath();
        var bin = GetCachedJavaBin();
        if (path == null || bin == null)
        {
            return false;
        }

        return await SystemHelper.TryExecuteJavaAsync(bin,
            arguments,
            path);
    }

    public async Task<bool> IsCachedJavaValid()
    {
        var path = GetCachedJavaPath();
        var bin = GetCachedJavaBin();

        if (path == null || !File.Exists(bin))
        {
            return false;
        }

        return await SystemHelper.IsJava8Async(SystemHelper.CreateStartInfo(bin, path, SystemHelper.VersionCall));
    }
}
