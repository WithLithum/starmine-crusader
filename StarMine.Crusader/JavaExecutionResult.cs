﻿namespace StarMine.Crusader;

public record struct JavaExecutionResult
{
    public JavaExecutionResult(bool succeeded, int exitCode)
    {
        Succeeded = succeeded;
        ExitCode = exitCode;
    }

    public bool Succeeded { get; set; }
    public int ExitCode { get; set; }

    public readonly bool IsSuccess => Succeeded && ExitCode == 0;

    public static JavaExecutionResult StartFailure()
    {
        return new JavaExecutionResult(false, -1);
    }
}
