﻿namespace StarMine.Crusader.Network;
using System.Collections.Generic;

internal class JavaReleaseMetadata
{
    public string Id { get; set; } = "<invalid>";
    public string ReleaseName { get; set; } = "release";
    public IList<JavaBinaryData> Binaries { get; set; } = [];
    public JavaVersionData VersionData { get; set; }
    public int DownloadCount { get; set; }
}
