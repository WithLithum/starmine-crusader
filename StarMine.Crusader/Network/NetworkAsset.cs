﻿namespace StarMine.Crusader.Network;

public record struct NetworkAsset
{
    public string Link { get; set; }
    public string Sha256 { get; set; }
    public string FileName { get; set; }
}
