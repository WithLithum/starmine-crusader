﻿namespace StarMine.Crusader.Network.Mirrors;
using System.Collections.Generic;

public record struct MirroredAsset
{
    public string Sha256 { get; set; }
    public string FileName { get; set; }
    public IEnumerable<MirrorInfo> Mirrors { get; set; }
}
