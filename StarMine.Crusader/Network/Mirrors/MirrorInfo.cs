﻿namespace StarMine.Crusader.Network.Mirrors;

public record struct MirrorInfo
{
    public string Name { get; set; }
    public string Link { get; set; }
}
