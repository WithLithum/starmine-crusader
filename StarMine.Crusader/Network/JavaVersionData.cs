﻿namespace StarMine.Crusader.Network;

public record struct JavaVersionData
{
    public int Major { get; set; }
    public int Minor { get; set; }
    public int Build { get; set; }
    public int Security { get; set; }
    public string Semver { get; set; }
    public string OpenjdkVersion { get; set; }
}
