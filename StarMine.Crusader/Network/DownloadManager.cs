﻿namespace StarMine.Crusader.Network;

using Serilog;
using Spectre.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

internal class DownloadManager
{
    public DownloadManager(string downloadDirectory)
    {
        DownloadDirectory = downloadDirectory;
    }

    public string DownloadDirectory { get; }

    /// <summary>
    /// Gets the path that the specified asset would be stored inside the downloads directory.
    /// </summary>
    /// <param name="asset">The asset.</param>
    /// <returns>The path that the asset will be stored at.</returns>
    public string GetAssetPath(string fileName)
    {
        return Path.Combine(DownloadDirectory, fileName);
    }

    public async Task<bool> TryResolveAssetInteractiveAsync(NetworkAsset asset)
    {
        if (await DoesAssetExistAsync(asset.FileName, asset.Sha256))
        {
            return true;
        }

        var progress = new Progress(AnsiConsole.Console);
        return await progress.StartAsync(async x =>
        {
            var task = new DownloadTask(asset, this, x);
            return await task.DownloadInteractiveAsync();
        });
    }

    public async Task<bool> DoesAssetExistAsync(string fileName, string sha256)
    {
        var file = GetAssetPath(fileName);

        if (File.Exists(file))
        {
            // This logic calculates existing file.
            var sha = SHA256.Create();
            byte[] hash;

            using (var stream = File.OpenRead(file))
            {
                hash = await sha.ComputeHashAsync(stream);
            }

            if (hash.Length != 0)
            {
                var localHash = BitConverter.ToString(hash).Replace("-", null);
                var assetHash = sha256;

                Log.Information("Local: {HashLocal}{NewLine}Asset:{HashRemote}", localHash, assetHash);

                if (localHash.Equals(assetHash, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
