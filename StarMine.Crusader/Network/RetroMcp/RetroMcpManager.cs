﻿namespace StarMine.Crusader.Network.RetroMcp;

using Microsoft.Extensions.Configuration;
using Serilog;
using StarMine.Crusader.Java;
using System.Threading.Tasks;

internal class RetroMcpManager
{
    private readonly DownloadManager _downloadManager;

    public RetroMcpManager(string workDirectory, DownloadManager downloadManager)
    {
        WorkDirectory = workDirectory;
        _downloadManager = downloadManager;
    }

    public string WorkDirectory { get; set; }

    private static NetworkAsset LoadRetroMcp()
    {
        return Program.Config.GetRequiredSection("RetroMCP").Get<NetworkAsset>();
    }

    public async Task<bool> ResolveRetroMcp()
    {
        var asset = LoadRetroMcp();
        if (asset == default)
        {
            Log.Error("RETROMCP ASSET CONFIGURATION IS MISSING!");
            Log.Error("Reset configuration by using reset_crusader script");
            return false;
        }

        return await _downloadManager.TryResolveAssetInteractiveAsync(asset);
    }

    public async Task<JavaExecutionResult> ExecuteMcp(string arguments, string workDir, IJavaProvider provider)
    {
        var asset = LoadRetroMcp();
        if (asset == default)
        {
            Log.Error("RETROMCP ASSET CONFIGURATION IS MISSING!");
            Log.Error("Reset configuration by using reset_crusader script");
            return JavaExecutionResult.StartFailure();
        }

        return await provider.ExecuteJavaAsync($"-jar {_downloadManager.GetAssetPath(asset.FileName)} {arguments}", workDir);
    }

    public bool IsSetupComplete()
    {
        var setupComplete = Path.Combine(WorkDirectory, "setup_complete.txt");

        return File.Exists(setupComplete);
    }

    public async Task<bool> TrySetupInteractiveAsync(IJavaProvider provider)
    {
        var setupComplete = Path.Combine(WorkDirectory, "setup_complete.txt");

        if (File.Exists(setupComplete))
        {
            return true;
        }

        var mcVersion = Program.Config.GetValue<string>("MCVersion") ?? "b1.7.3";

        if (!await ResolveRetroMcp())
        {
            Log.Error("Unable to resolve RetroMCP");
            return false;
        }

        var success = (await ExecuteMcp($"setup {mcVersion}", WorkDirectory, provider)).IsSuccess;

        if (success)
        {
            File.WriteAllText(setupComplete, "This file is used to indicate that Crusader have set up your RetroMCP environment.");
        }

        return success;
    }
}
