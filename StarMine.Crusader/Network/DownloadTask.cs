﻿namespace StarMine.Crusader.Network;
using Downloader;
using Serilog;

using Spectre.Console;
using System;
using System.Threading.Tasks;

internal class DownloadTask
{
    private readonly ProgressContext _context;
    private ProgressTask? _task;
    private bool complete;
    private readonly NetworkAsset _asset;
    private readonly DownloadManager _manager;

    internal DownloadTask(NetworkAsset asset, DownloadManager manager, ProgressContext context)
    {
        _asset = asset;
        _manager = manager;
        _context = context;
    }

    private static readonly DownloadConfiguration downloadOpt = new()
    {
        ChunkCount = 5,
        ParallelDownload = true,
        CheckDiskSizeBeforeDownload = true,
        RequestConfiguration =
        {
            UserAgent = "starmine-crusader/0.1.0-alpha"
        }
    };

    internal async Task<bool> DownloadInteractiveAsync()
    {
        var downloader = new DownloadService(downloadOpt);
        var target = _manager.GetAssetPath(_asset.FileName);

        _task = _context.AddTask("Downloading");
        downloader.DownloadProgressChanged += Downloader_DownloadProgressChanged;
        downloader.DownloadFileCompleted += Downloader_DownloadFileCompleted;

        Log.Information("Downloading ({Link}) to ({Target})", _asset.Link, target);

        try
        {
            File.WriteAllBytes(target, []);
            await downloader.DownloadFileTaskAsync(_asset.Link, target);
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Failed to download {Link}", _asset.Link);
            return false;
        }

        while (!complete)
        {
            // do nothing
        }

        return true;
    }

    private void Downloader_DownloadFileCompleted(object? sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        _task?.StopTask();
        _task = null;
        complete = true;
    }

    private void Downloader_DownloadProgressChanged(object? sender, DownloadProgressChangedEventArgs e)
    {
        if (_task == null)
        {
            return;
        }

        _task.Value = e.ProgressPercentage;
    }
}
