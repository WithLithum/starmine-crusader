﻿namespace StarMine.Crusader.Network;

using System.Text.Json.Serialization;

public record class JavaBinaryData
{
    public string Architecture { get; set; } = "x64";
    public int DownloadCount { get; set; }
    public string HeapSize { get; set; } = "normal";
    public string ImageType { get; set; } = "jdk";
    public string JvmImpl { get; set; } = "hotspot";
    public string Os { get; set; } = "os";

    [JsonPropertyName("package")]
    public JavaDownloadData Package { get; set; }
}
