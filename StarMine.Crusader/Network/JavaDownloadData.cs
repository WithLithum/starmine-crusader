﻿namespace StarMine.Crusader.Network;

public struct JavaDownloadData
{
    public string Checksum { get; set; }
    public Uri ChecksumLink { get; set; }
    public Uri Link { get; set; }
    public Uri MetadataLink { get; set; }
    public string Name { get; set; }
    public Uri SignatureLink { get; set; }
    public long Size { get; set; }
    public int DownloadCount { get; set; }
}
