﻿namespace StarMine.Crusader;

using Microsoft.Extensions.Configuration;
using StarMine.Crusader.Java;
using StarMine.Crusader.Network;
using StarMine.Crusader.Network.RetroMcp;

internal class CrusaderApp
{
    internal static readonly string AppPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
    "starmine",
    "crusader");

    internal static readonly string McpPath = Path.Combine(AppPath, "mcp");

    public CrusaderApp(IConfiguration configuration)
    {
        SystemJavaSearcher = new SystemJavaSearcher(configuration.GetValue<string?>("SystemJavaName", null) ?? "java",
           configuration.GetValue<string>("SystemJavaHome"));
        JavaCacheManager = new JavaCacheManager(configuration.GetValue<string?>("JavaCacheDirectory", null));
        DownloadManager = new DownloadManager(configuration.GetValue<string?>("JavaDownloadDirectory", null)
            ?? JavaInstaller.DefaultDownloadsDir);
        JavaInstaller = new JavaInstaller(JavaCacheManager, DownloadManager);

        RetroMcpManager = new RetroMcpManager(McpPath, DownloadManager);
    }

    public DownloadManager DownloadManager { get; }
    public JavaInstaller JavaInstaller { get; }
    public JavaCacheManager JavaCacheManager { get; }
    public SystemJavaSearcher SystemJavaSearcher { get; }

    public RetroMcpManager RetroMcpManager { get; }
}
